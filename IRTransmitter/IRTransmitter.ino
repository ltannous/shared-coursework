//Arduino-IRremote library will be added
#include <IRremote.h>
#include <IRremoteInt.h>

//...and here initialized
IRsend irsend;

int Xvalue;
int Yvalue;
int Bvalue;

int counter = 0;


void setup()
{
  pinMode(2, INPUT_PULLUP); // we use a pullup-resistor for the button functionality
  Serial.begin(9600);
}

void loop()
{
  Xvalue = analogRead(A0);  // read X axis value [0..1023]
  Serial.print("X:");
  Serial.print(Xvalue, DEC);

  Yvalue = analogRead(A1); // read Y axis value [0..1023]
  Serial.print(" | Y:");
  Serial.print(Yvalue, DEC);

  Bvalue = digitalRead(2); // read joystick Button state [0,1]
  Serial.print(" | Button:");
  Serial.println(Bvalue, DEC);

  if (Bvalue == 0)
  {
    counter += 1;
  }


  delay(100);

  if (counter == 1) {
    Serial.println("I am inside");

    if (Xvalue <= 10) {
      irsend.sendRC5(0xA90, 12); // [0xA90] signal for forward
      delay(100);
    }
    else if (Xvalue >= 1000) {
      irsend.sendRC5(0xA91, 12);// [0xA91] signal for backward
      delay(100);
    }
    else if (Yvalue <= 10) {
      irsend.sendRC5(0xA92, 12);// [0xA92] signal for Right
      delay(100);
    }
    else if ( Yvalue >= 1000) {
      irsend.sendRC5(0xA93, 12);// [0xA93] signal for Left
      delay(100);
    }
    else {
      irsend.sendRC5(0xA94, 12);// [0xA94] signal for stop
      delay(100);
    }
  }
  else if (counter == 2) {
    Serial.println("I am Out");
    irsend.sendRC5(0xA96, 12); // [0xA96] signal for not using remote control
    delay(100);
    counter = 0;
  }
  delay(100);


}
