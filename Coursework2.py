#https://www.pyimagesearch.com/2015/01/19/find-distance-camera-objectmarker-using-python-opencv/
import numpy as np
from matplotlib import pyplot
import cv2
from matplotlib import cm
from matplotlib import colors
import importlib
import time
import sys
importlib.import_module('mpl_toolkits.mplot3d').__path__
import serial
import imutils


port = '/dev/ttyUSB0'
ser = serial.Serial(port, 9600)

# initialize the known distance from the camera to the object, which
# in this case is 1meter
KNOWN_DISTANCE = 1

# initialize the known object width, which in this case, the piece of
# box is 14.5cm = 0.145 meter wide
KNOWN_WIDTH = 0.145


def findContours(image):
    #print(image)
    rgb_img = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    hsv_img = cv2.cvtColor(rgb_img, cv2.COLOR_RGB2HSV)
        
    ## getting black HSV color representation for low and high color
    black_low = (18,44,31)
    black_high = (33, 255, 255)

    black_mask = cv2.inRange(hsv_img, black_low, black_high)

    result = cv2.bitwise_and(image, image, mask=black_mask)
    #print(result)
    # convert the image to grayscale, blur it, and detect edges
    gray = cv2.cvtColor(result, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (3, 3), 0)

    # find the contours and keep the largest one;
    # we'll assume that this is our box in the image
    cnts = cv2.findContours(gray, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    #print(cnts)
    cnts = imutils.grab_contours(cnts)
    c = max(cnts, key = cv2.contourArea)

    return cv2.minAreaRect(c)
 
            
    


def distance_to_camera(knownWidth, focalLength, perWidth):
    # compute and return the distance from the maker to the camera
    return (knownWidth * focalLength) / perWidth


#"video frames capture 

cap = cv2.VideoCapture(0)
# We give some time for the camera to warm-up!
    

def display_video():
  # Exit if video not opened.
  if cap.isOpened():

    # Read a new frame
    ok, frame = cap.read()

    if ok:
      # load the first image that contains an object that is KNOWN TO BE 0.145 meter width and 1 meter far
      # from our camera, then find the box in the image, and initialize the focal length
     cont = findContours(frame)
     focalLength = (cont[1][0] * KNOWN_DISTANCE) / KNOWN_WIDTH
     print(focalLength)
     while True:
      ok, frame = cap.read()


      if ok: 
     
           # # Start timer
           timer = cv2.getTickCount()
           time.sleep(0.02)

           # load the image, find the marker in the image, then compute the
           # distance to the marker from the camera
           if (findContours(frame) != None):
               contour = findContours(frame)
               cm = distance_to_camera(KNOWN_WIDTH, focalLength, contour[1][0])*10
               print(cm)
               # draw a bounding box around the image and display it
               box = cv2.cv.BoxPoints(contour) if imutils.is_cv2() else cv2.boxPoints(contour)
               box = np.int0(box)
               cv2.drawContours(frame, [box], -1, (0, 255, 0), 2)
               cv2.putText(frame,"%.2fft" % (cm / 12) ,(frame.shape[1] - 200, frame.shape[0] - 20), cv2.FONT_HERSHEY_SIMPLEX,2.0, (0, 255, 0), 3)


               #checking distance and write the direction to the serial
               if (cm > 40):
                   ser.write(str.encode('F'))
                   cv2.waitKey(3)
               elif (cm <= 40):
                   ser.write(str.encode('S'))
                   cv2.waitKey(3)
                   ser.write(str.encode('R'))
                   cv2.waitKey(3)
                

           cv2.imshow("image", frame)                                                 
           cv2.waitKey(1)

           if cv2.waitKey(20) == ord('q'):  # Introduce 20 milisecond delay.  press q to exit.
               break
    else:
      print('Cannot read video file')
      sys.exit()
  else:
    print("Could not open video")
    sys.exit()



display_video()

cv2.destroyAllWindows()
