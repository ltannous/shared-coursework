#include <IRremote.h>
#include <IRremoteInt.h>

int RECV_PIN = 2; // define input pin on Arduino
IRrecv irrecv(RECV_PIN);
decode_results results; // decode_results class is defined in IRremote.h

#define pwmA 12
#define pwmB 12
#define in1 6
#define in2 7
#define in3 8
#define in4 9

char receivedChar;
int pwmOutputA = 180;
int pwmOutputB = 180;

void setup()
{
  pinMode(pwmA, OUTPUT);
  pinMode(pwmB, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);
  pinMode(3, OUTPUT);
  Serial.begin(9600);
  irrecv.enableIRIn(); // Start the receiver

}

void loop()
{
  if (irrecv.decode(&results))
  {
    Serial.println(results.value, HEX);
    if (results.value == 0xA96)
    {
      recvOneChar();
    } 
    else {
      //bot moves front
      if (results.value == 0xA90)
      {
        moveforward();
      }
      //bot moves back
      if (results.value == 0xA91)
      {
        movebackward();
      }
      //bot moves left
      if (results.value == 0xA93)
      {
        turnleft();

      }
      //bot moves right
      if (results.value == 0xA92)
      {
        turnright();
      }
      //bot stops
      if (results.value == 0xA94)
      {
        stops();
      }
    }
    irrecv.resume(); // Receive the next value
  }
  else {
    recvOneChar();
  }
}


void moveforward() {
  analogWrite(pwmA, pwmOutputA);
  analogWrite(pwmB, pwmOutputB);
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
}

void movebackward() {
  analogWrite(pwmA, pwmOutputA);
  analogWrite(pwmB, pwmOutputB);
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
}

void turnleft() {
  analogWrite(pwmA, pwmOutputA);
  analogWrite(pwmB, pwmOutputB);
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
}

void turnright() {
  analogWrite(pwmA, pwmOutputA);
  analogWrite(pwmB, pwmOutputB);
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
}

void stops() {
  analogWrite(pwmA, pwmOutputA);
  analogWrite(pwmB, pwmOutputB);
  digitalWrite(in1, HIGH);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, HIGH);
}

void recvOneChar() {
  if (Serial.available() > 0) {
    receivedChar = Serial.read();
    Serial.println(receivedChar);
    if (receivedChar == 'F'){
      moveforward();
    } 
    else if (receivedChar == 'S'){
      stops();
    }
    else if (receivedChar == 'R'){
      turnright();
      moveforward();
      turnleft();
    }
  }
}
